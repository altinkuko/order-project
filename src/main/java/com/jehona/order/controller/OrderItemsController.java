package com.jehona.order.controller;

import com.jehona.order.models.OrderItems;
import com.jehona.order.services.OrderItemsService;
import com.jehona.order.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class OrderItemsController {
    @Autowired
    private OrderItemsService orderItemsService;
    @Autowired
    private UserController userController;
    @Autowired
    private ProductServices productServices;

    @GetMapping("/cart")
    public String getOrderItems(Model model) {
        model.addAttribute("user", this.userController.getCurrentUser());
        List<OrderItems> orderItems = this.orderItemsService.getOrdersByUser(this.userController.getCurrentUser());
        model.addAttribute("orderItems", orderItems);
        return "cart";
    }

    @PostMapping("/cart")
    public String buyItem(@Valid OrderItems orderItem, Model model, @RequestParam Long productId) {
        orderItem.setProduct(this.productServices.getProductById(productId));
        orderItem.setUser(this.userController.getCurrentUser());
        this.orderItemsService.saveOrderItem(orderItem);
        model.addAttribute("user", this.userController.getCurrentUser());
        model.addAttribute("cartItem", orderItem);
        List<OrderItems> orderItems = this.orderItemsService.getOrdersByUser(this.userController.getCurrentUser());
        model.addAttribute("orderItems", orderItems);
        return "cart";
    }

    @GetMapping("/deleteOrder/{orderId}")
    public String deleteOrder(@PathVariable("orderId") Long orderId, Model model) {
        orderItemsService.deleteOrder(orderId);
        model.addAttribute("orderItems", this.orderItemsService.getOrdersByUser(this.userController.getCurrentUser()));
        return "redirect:/cart";
    }
}
