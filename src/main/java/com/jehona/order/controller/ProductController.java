package com.jehona.order.controller;

import com.jehona.order.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductController {
    @Autowired
    private ProductServices productServices;
    @Autowired
    private UserController userController;

    @GetMapping("/products")
    public String listAllProducts(Model model) {
        model.addAttribute("products", this.productServices.getAllProducts());
        System.out.println("listing products");
        return "index";
   }
}
