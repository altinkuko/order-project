package com.jehona.order.controller;

import com.jehona.order.models.OrderItems;
import com.jehona.order.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfileController {
    @Autowired
    private ProductServices productServices;

    @GetMapping({"/", "/index"})
    public String root(Model model, @AuthenticationPrincipal UserDetails currentUser) {
        model.addAttribute("user", currentUser);
        model.addAttribute("products", this.productServices.getAllProducts());
        model.addAttribute("cartItem", new OrderItems());
        return "index";
    }
    }