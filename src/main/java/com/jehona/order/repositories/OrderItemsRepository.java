package com.jehona.order.repositories;

import com.jehona.order.models.OrderItems;
import com.jehona.order.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemsRepository extends JpaRepository<OrderItems, Long> {

    public List<OrderItems> getOrderItemsByUser (User user);

}
