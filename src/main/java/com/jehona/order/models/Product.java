package com.jehona.order.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Must have a name")
    private String productName;
    private String productDescription;
    private Long productPrice;
    private String imageUrl;
    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<OrderItems> orderItems;
}
