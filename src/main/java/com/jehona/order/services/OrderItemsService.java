package com.jehona.order.services;

import com.jehona.order.models.OrderItems;
import com.jehona.order.models.User;
import com.jehona.order.repositories.OrderItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemsService {
    @Autowired
    private OrderItemsRepository orderItemsRepository;

    public List<OrderItems> getOrdersByUser(User user){
        return this.orderItemsRepository.getOrderItemsByUser(user);
    }
    public OrderItems saveOrderItem(OrderItems orderItems){
        return this.orderItemsRepository.save(orderItems);
    }
    public void deleteOrder(Long id){
        this.orderItemsRepository.deleteById(id);
    }
}
